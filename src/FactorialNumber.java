
public class FactorialNumber {

            public static void main(String[] args) {
                        // TODO Auto-generated method stub
                        System.out.println(iterativeFactorial(5));
                        System.out.println(recursiveFactorial(20));

            }
           //using iterations
            public static int iterativeFactorial(int num) {
                        if(num == 0) {
                                    return 1;
                        }
                        int factorial = 1;
                        for(int i=1;i<=num; i++) {
                                    factorial = factorial*i;
                                    //factorial *=i;
                        }
                        return factorial;
            }
            //using recursions
            public static int recursiveFactorial(int num) {
                        if(num ==0) {
                                    return 1;
                        }
                        int factorial = num * recursiveFactorial(num-1);
                        return factorial;
                        
            }

}
