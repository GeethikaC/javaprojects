import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmployeeCollection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee e1 = new Employee(101, "Geethika1", 400000.05f ,"IT");
		Employee e2 = new Employee(102, "Geethika2", 400000.05f ,"IT");
		Employee e3 = new Employee(103, "Geethika3", 400000.05f ,"IT");
		Employee e4 = new Employee(104, "Geethika4", 400000.05f ,"IT");
		List<Employee> employeeList = new ArrayList<Employee>(); 
		
		employeeList.add(e1);
		employeeList.add(e2);
		employeeList.add(e3);
		employeeList.add(1,e4);
		System.out.println(employeeList); //returns memory value
		System.out.println(employeeList.get(3).empName);//returns empName value from arrayList at index=3
		System.out.println("\n");
		Iterator<Employee> empIterator = employeeList.iterator();
		while(empIterator.hasNext()) {
			System.out.println(empIterator.next().empName);
	
		}
		
/*for(int i=0;i<employeeList.size();i++) {
		
		System.out.println(employeeList.get(i).empName);
		}*/
		/*for (Employee employee : employeeList) {
			
			System.out.println(employee.empName + " " + employee.empNo+ " " + employee.empDept + " " + employee.empSal);
		}*/
employeeList.forEach((Employee)-> 
{System.out.println(Employee.empName + " " + Employee.empNo+ " " + Employee.empdept + " " + Employee.empSal);});
		
	}


}
