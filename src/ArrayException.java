
public class ArrayException {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//type variable name = new type[size]
		try{
			int intArray[] = new int[4];
			//int intArray[] = new int[] {0,1,2,3};
		//or
		int[] intArray2; 
		
		byte byteArray[];
		//or
		byte[] byteArray2[]; 
		
		ArrayException employeeArray[]; //non-primitive type array
		Object objectArray[]; 
		
		intArray[0] = 100;
		intArray[1] = 200;
		intArray[2] = 300;
		intArray[3] = 400;
		 
		for(int index =0;index<intArray.length;index++) {
			System.out.println(intArray[index]);
			
		}
		//not allowed as the array is defined for size=2
				intArray[4] = 500;
		
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Error Handled");
		}
	}

}
