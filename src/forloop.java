//if last part of for conditions are missing then it's an infinity loop
//example of infinity loop  -> for (;;)

public class forloop {
	public static void main(String[] args) {
		//for(init i ; condition ; inc/dec) {}
		for (int n =1; n<5; n++) //can also be written as for (; n<5; n=n+1)
		{
			System.out.println("The number"+n);
		}
		
		
	}

}
