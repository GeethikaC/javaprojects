
public class wrapperclasses {
	
	public static void main (String[] args) {
		//converting int primitive data type to object type
		int number = 100;
		Integer object = Integer.valueOf(number);//converting to Integer object type 
		System.out.println(number + "\n"+object);

		//converting Integer object type to primitive
		Integer object2 = new Integer(100);
		int num = object2.intValue();//converting to int primitive data type 
		System.out.println(object2+"\n"+num);
		
		//converting float primitive data type to object type
				float fnumber = 100.2f;
				Float fobj = Float.valueOf(fnumber);
				System.out.println(fnumber + "\n"+fobj);

				//converting Float object type to primitive
				Float fobj2 = new Float(100.23f);
				float fnum = fobj2.floatValue();
				System.out.println(fobj2+"\n"+fnum);
			
				//converting double primitive data type to object type
				double dnumber = 1000.232d;
				Double dobj = Double.valueOf(dnumber);
				System.out.println(dnumber + "\n"+dobj);

				//converting Double object type to primitive
				Double dobj2 = new Double(10023.343d);
				double dnum = dobj2.doubleValue();
				System.out.println(dobj2+"\n"+dnum);
		
		//autoboxing
		int number2 = 1934;
		Integer objnum = number2; //converting to Integer object type implicitly
		System.out.println(number2+"\n"+objnum);
		
		//auto-unboxing
		int num2= new Integer(103);//converting to int primitive data type implicitly
		Integer objectnum = new Integer(105);
		int num3 = objectnum;//converting to data primitive data type implicitly
		
		System.out.println(num2);
		System.out.println(objectnum+"\n"+num3);
	}

}
