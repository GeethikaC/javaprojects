import java.util.Scanner;

public class Whileloop {
	
	public static void main(String[] args) {
		
		
		/*Infinity loop
		 int n =0;
		 while(n<5) {System.out.println("I am in an Infinity loop")}; */
		
		
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the number of times you want to type:");
		int n = s.nextInt();
		int i=0;
		while(i<n) {
			System.out.println("Hi "+i);
			i++;
			
		}
	}

}
