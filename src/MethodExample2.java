
public class MethodExample2 {
	int a ;
	int b;
	
	//MethodExample2(){} -> no arg. constructor/zero parameterized constructor/default constructor
	
	MethodExample2(int x, int y){
		this.a =x;
		this.b = y;
	}
	
	void showData() {
		System.out.println("A is "+this.a);
		System.out.println("B is "+this.b);
	}
	
	static void myMethod() {
		System.out.println("Void Non Parameterized Function");
	}
	
	static void yourMethod(int number) {
		System.out.println("Void Parameterized function");
		System.out.println("Param"+number);
	}
	
	static float weMethod(int number,float fNumber) {
		System.out.println("Void type 2 Parameterized function");
		System.out.println("Param 1: "+number);
		System.out.println("Param 2:"+fNumber);
		return fNumber;
	}
	static int ourMethod() {
		System.out.println("Void Non Parameterized function");
		return 10;
	}
	
	public static void main(String[] args) {
		myMethod();
		yourMethod(2);
		weMethod(2,2.0f);
		ourMethod();
		
	}
}
