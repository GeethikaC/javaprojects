
public class StringEx {
	
	public static void main(String[] args) {
		//1. string literal
		//2. String object
		
		String str1 = "operator"; //string literal
		char chr[] = {'o','p','e','r','a','t','o','r'};
		
		String str2 =  new String("operator1"); //String object -> String with 'NEW' keyword
		
		System.out.println(str1);
		System.out.println(str2);
		
		
	}

}
