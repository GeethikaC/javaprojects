
public class ArrayDS {

	public static void main(String[] args) {
		int[]intArray = new int[7];
		intArray[0] = 20;
		intArray[1] = 35;
		intArray[2] = 20;
		intArray[3] = -11;
		intArray[4] = 60;
		intArray[5] = 90;
		intArray[6] = 50;
		// Forward iteration
		for (int i =0 ; i<intArray.length;i++) {
			System.out.println("index :"+i +"value:"+intArray[i]);
		}
		System.out.println("----------**********----------");
		// reverse iteration
		
		for (int i = intArray.length-1;i>0;i--) {
			System.out.println("index :"+i +"value:"+intArray[i]);
	}
	
	}
	
} 
 

