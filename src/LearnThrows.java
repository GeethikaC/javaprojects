import java.io.IOException;

public class LearnThrows {

	static int balance =10000;
	public static void deposit(int amount) throws IOException {
		//try {
		if(amount <0) {
			throw new IOException("Invaid Amount in input");
		}else {
			balance = balance + amount;
			System.out.println(balance);
		}
//	}catch(Exception e) {}
	}
	public static void main(String[] args) throws IOException {
	//or public static void main(String[] args) throws IOException instead of try catch inside
		// TODO Auto-generated method stub
try{
	deposit(-1000);
}catch(Exception e) {
	System.out.println("Error Handled");
}
System.out.println("executed");
	}

}
