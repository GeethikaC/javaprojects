
public class MethodExampleInvocation {

	public static void main(String[] args) {
		//static functions can be called without a new object created
		//MethodExample2 is a example for constructor
		MethodExample2.myMethod();
		MethodExample2.yourMethod(2);
		MethodExample2.weMethod(2,2.0f);
		MethodExample2.ourMethod();
		MethodExample2 me = new MethodExample2(2,3); //parameterized constructor 
		me.showData();
		int z = me.a;
		System.out.println(z);
	}
}
