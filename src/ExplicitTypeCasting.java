
public class ExplicitTypeCasting {
	
	public static void main(String[] args) {
		int i = 20;
		double d = 100.0d;
		long l = (long) d;
		int i1 = (int) l;
		double d1 = d +l;
		long l1 = l + (long) d1;
		byte b = 10;
		int i2 = b + i + (int)d + (int)(l + l1) + i1 + (int)d1;
		
		System.out.println(l);
		System.out.println(i1);
		System.out.println(d1);
		System.out.println(l1);
		System.out.println(i2);
		
	}
	
}
