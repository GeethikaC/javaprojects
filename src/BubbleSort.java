
public class BubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[]  intArray = {20,35,-15,7,55,1,-22};
		//logic for bubble sort
		for(int lastSortedindex = intArray.length -1; lastSortedindex>0;
			lastSortedindex--) {
			for(int i=0;i<lastSortedindex;i++) {
	//			System.out.println("index: "+i+" value:"+intArray[i]);
				if(intArray[i]>intArray[i+1])
					swap(intArray,1,i+1);
				
			}
		}
		
		//for printing
		for(int i=0;i<intArray.length;i++)
			System.out.println("index: "+i+" value:"+intArray[i]);

	}

	private static void swap(int[] array, int i, int j) {
		// TODO Auto-generated method stub
		int tmp = array[i];
		array[i] = array[j];
		array[j] = tmp;		
	}

}
