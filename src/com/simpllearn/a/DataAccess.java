package com.simpllearn.a;

public class DataAccess {
	
	//accessible everywhere
	public static int publicV ;
	//Default is accessible within a package
	static int defaultV;
	//protected is accessible within a class,package,outside the package with the help of inheritance
	protected static int protectedV;
	//private is accessible within a class only
	private static int privateV;
	
	//Inner class
	public class PrivateClass{}
	class DefaultClass{}
	protected class ProtectedClass{}
	public class PublicClass{}
	
	private static void privatemethod() {};
	static void defaultmethod() {};
	protected static int protectedmethod() {return 10;};
	public static void publicmethod() {};
	
	public static void main(String[] args) {
		System.out.println("data"+privateV);
		System.out.println("data"+defaultV);
		System.out.println("data"+protectedV);
		System.out.println("data"+publicV);
		
		
	}
		
	}


