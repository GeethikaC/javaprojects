package com.simpllearn.polymorphism;

public class Mainmethodoverloading {
	//String args is always called by JVM
	public static void main(String[] args) {
		System.out.println("main(String[] args)");
		main(2);
	}
	
	public static void main(String args) {
		System.out.println("main(String args)");
	}

	public static void main() {
		System.out.println("main()");
	}
	
	public static void main(int args) {
		System.out.println("main(int args)");
	}
}
