package com.simpllearn.polymorphism;

public class bankaxisinterestrate extends bankinterestoverriding {
	
	public static void calculateinterest() {
		int principle = 1000 ; 
		float intperc = 9.5f;
		float intamt = intperc * principle/100 ; 
		System.out.println("Interest Amount in AXIS is: " + intamt);
	}
	
	public static void main(String[] args) {
		bankaxisinterestrate AXISrate = new bankaxisinterestrate();
		AXISrate.calculateinterest();
	}

}
