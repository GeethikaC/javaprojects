package com.simpllearn.polymorphism;

public class bankhdfcinterestrate extends bankinterestoverriding {

	public static void calculateinterest() {
		int principle = 1000 ; 
		float intperc = 10.5f;
		float intamt = intperc * principle/100 ; 
		System.out.println("Interest Amount in HDFC is: " + intamt);
	}
	
	public static void main(String[] args) {
		bankhdfcinterestrate hdfcrate = new bankhdfcinterestrate();
		hdfcrate.calculateinterest();
	}
}
