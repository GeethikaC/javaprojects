package com.simpllearn.polymorphism;

public class dellmarathonoverriding extends marathonoverriding {
	//overriding run method
	public static void run() {
		int a = 10;
		System.out.println("running for "+a+" miles");
	}
	
	public static void main(String[] args) {
		dellmarathonoverriding dellm = new dellmarathonoverriding();
		marathonoverriding dellM = new marathonoverriding();
		dellM.run(); //calling directly from super class
		dellm.run();  //calling the overridden method
		dellm.walk(); //calling without overriding
	}

}
