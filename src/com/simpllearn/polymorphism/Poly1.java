package  com.simpllearn.polymorphism;

public class Poly1 {
	//method overloadng will be performed within
	//1. by changing the number of argument
	//2. by changing the parameter datatype
	
	public static void takeorder() {
		//logic
		System.out.println("always give coffee");
	}
	
	private static void takeorder(int price) {
		if(price >20) {
			System.out.println("Server Tea");
			//return price;
		}if(price <20) {
			System.out.println("server latte");
			//return price; 
		}if(price==20) {
			System.out.println("serve capuccino");
			//return price;
		}
		//return price; 
	}
	//return type has to be near function but rest can be changed
	static public void takeorder(float price, int time) {
		if(price>=20 && time >20) {
			System.out.println("serve whatever you have");			
		}else if(price >= 800 && time >40) {
			System.out.println("serve freshly made");		
		}
	}

	public static void main(String[] args) {
		//TODO Auto-generated method stub
		takeorder(20f,30);//method overloading
		
	}
}
