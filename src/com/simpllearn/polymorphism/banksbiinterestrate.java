package com.simpllearn.polymorphism;

public class banksbiinterestrate extends bankinterestoverriding {
	
	public static void calculateinterest() {
		int principle = 1000 ; 
		float intperc = 8.5f;
		float intamt = intperc * principle/100 ; 
		System.out.println("Interest Amount in SBI is: " + intamt);
	}
	
	public static void main(String[] args) {
		banksbiinterestrate SBIrate = new banksbiinterestrate();
		SBIrate.calculateinterest();
	}
}
