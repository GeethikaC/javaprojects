package com.simpllearn.abstraction;

public interface Action {
	int x=10;
	abstract void fighting();
	abstract void dancing();	
	abstract void crying();
	
}
