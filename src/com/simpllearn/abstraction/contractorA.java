package com.simpllearn.abstraction;

public class contractorA extends architecture{

	
	@Override
	 void plan() {
		// TODO Auto-generated method stub
		System.out.println("planning A");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		contractorA arch = new contractorA();
		arch.plan();
		arch.design();
		
		architecture arch2 = new contractorA(); //taking reference of super class and object of sub class
		arch2.plan();
		arch2.design();
	}

}
