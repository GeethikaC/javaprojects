package com.simpllearn.abstraction;

//to achieve partial abstraction
public abstract class architecture {
	
    //implemented method / non- abstract method
	public void design() {
		System.out.println("designing done");
		
	}
	//unimplemented method / abstract method
	abstract void plan();
	

}
