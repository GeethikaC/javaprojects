package com.simpllearn.abstraction;

public class Hero implements Action {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Action act = new Hero();
		act.fighting();
		act.dancing();
		act.crying();
	}

	@Override
	public void fighting() {
		// TODO Auto-generated method stub
		System.out.println("Fighting: mandatory");
	}

	@Override
	public void dancing() {
		// TODO Auto-generated method stub
		System.out.println("Dancing: not in this movie");
	}

	@Override
	public void crying() {
		// TODO Auto-generated method stub
		System.out.println("Crying: hero does not cry!");
	}

}
