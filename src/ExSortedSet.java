
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class ExSortedSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeSet<Integer> uniqueDigits = new TreeSet<Integer>();
		
		uniqueDigits.add(10);
		uniqueDigits.add(110);
		uniqueDigits.add(180);
		uniqueDigits.add(10);
		uniqueDigits.add(30);
		System.out.println(uniqueDigits);//ascending order by default
		
		TreeSet<String> uniqueWords = new TreeSet<String>();
		
		uniqueWords.add("Geethika");
		uniqueWords.add("Sidhartha");
		uniqueWords.add("Prasad");
		uniqueWords.add("madhavi");
		
		System.out.println(uniqueWords); //in ascending order of ASCII value 
		System.out.println(uniqueWords.descendingSet());//in descending order of ASCII value
		
		Iterator<String> uniqueStringIterator = uniqueWords.descendingIterator(); //iterator-> ascending iterator & descendingiterator-> descending iterator
		//System.out.println(uniqueIterator);
		while(uniqueStringIterator.hasNext()){
			System.out.println(uniqueStringIterator.next());
		}
	}	
}