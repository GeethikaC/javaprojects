
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class Listcollectionexample {

	public static void main(String[] args) {
		//arraylist <datatype> collection_name = new ArrayList <datatye>();
		
		ArrayList<Integer> bleh = new ArrayList<Integer>(Arrays.asList(101,102,103,104,105,106,107,108,109,110));
		
		//or
		
		//ArrayList<Integer> bleh1 = new ArrayList<Integer>(Arrays.asList(101,102,103,104,105,106,107,108,109,110));
		
		//add method for adding elements in the List
	
		/*bleh.add(101);
		bleh.add(102);
		bleh.add(103);
		bleh.add(104);
		bleh.add(105);
		bleh.add(106);
		bleh.add(107);
		bleh.add(108);
		bleh.add(109);
		bleh.add(110);*/
		
		//or
		//bleh.addAll(bleh1);
		
		System.out.println(bleh);
		
		//replacing element at an index
				bleh.set(5, 200);
				System.out.println(bleh);
		
				//adding
		bleh.add(5, 201);
		System.out.println(bleh);
		
		//remove
		bleh.remove(6);
		System.out.println(bleh);
		
		//iterate ArrayList
		Iterator<Integer> blehIterator = bleh.iterator();
		
		while(blehIterator.hasNext()){
			System.out.println(blehIterator.next());
		}
	
 	}
}
