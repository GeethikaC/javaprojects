
public class unaryoperator {

	public static void main(String[] args) {
		int a =20;
		System.out.println(a);
		System.out.println(a++);
		System.out.println(++a);
		
		System.out.println("\n");
		int b = a;
		System.out.println(b);
		System.out.println(b++);
		System.out.println(--b);
		System.out.println(++b);
		System.out.println(b++);
		System.out.println(b--);
		
		System.out.println("\n");
		float f = b;
		System.out.println(f); //23.0
		//System.out.println(f+1); 
		f = f+10+4;
		System.out.println(f); 
		System.out.println(f++); 
		System.out.println(--f); 
		
		System.out.println("\n");
		
		//short notations
		int w = (int)(f+f);
		System.out.println(w);
		f+=f; //f = f+f
		//f+=1; //f = f+1
		f -= 1; //f =f-1
		System.out.println(f);
	
	}
}
