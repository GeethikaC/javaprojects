import java.util.Scanner;

public class SwitchCases {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the choice from Admin/vendor/user");
		String choice = s.next();
		switch(choice) {
		case "Admin":
		case "1":
		case "admin":
			System.out.print("Enter Admin name:");
			String Aname = s.next();
			System.out.print("Enter Admin ID:");
			String AID = s.next();
			System.out.print("Enter Admin department:");
			String Adept = s.next();
			System.out.println("\n"+"Admin name:"+Aname+"; Admin ID:"+AID+"; Admin department:"+Adept);
			break;
		
		case "user":
		case "2":
		case "User":
			System.out.print("Enter user name:");
			String uname = s.next();
			System.out.print("Enter user Age:");
			int uage = s.nextInt();
			System.out.print("Enter user ID:");
			String uID = s.next();
			System.out.println("\n"+"user name:"+uname+"; user Age:"+uage+"; user ID:"+uID);
			break;
			
		case "vendor":
		case "3":
		case "Vendor":
			System.out.print("Enter vendor name:");
			String vname = s.next();
			System.out.print("Enter vendor Area:");
			String varea = s.next();
			System.out.print("Enter vendor ID:");
			String vID = s.next();
			System.out.println("\n"+"vendor name:"+vname+"; vendor Area:"+varea+"; vendor ID:"+vID);
			break;
			
		default:
			System.out.println("Invalid Option");
			break;
		}
	}

}
