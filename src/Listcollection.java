
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.Vector;


import java.util.ArrayList;
import java.util.Iterator;

public class Listcollection {

	public static void main(String[] args) {
		//arraylist <datatype> collection_name = new ArrayList <datatye>();
		
		//ArrayList<String> empNames = new ArrayList<String>();
		//or
		List<String> empNames = new ArrayList<String>();  //using "list" type reference 
		//taking super-class reference but instantiating sub-class
		/*List<String> empNames1 = new LinkedList<String>();
		List<String> empNames2 = new Vector<String>();
		List<String> empNames3 = new Stack<String>();*/
		
		//add method for adding elements in the List
		empNames.add("Geet");
		empNames.add("Geet");
		empNames.add("Geet1");
		empNames.add("Geet2");
		
		System.out.println(empNames);
		
		//add an element at a particular index
		//Note: remember shifting of element
		empNames.add(3, "Added@3");
		System.out.println(empNames);
		
		//replacing element at an index
		empNames.set(3, "Replaced@3");
		System.out.println(empNames);
		
		//printing value at a certain index value
		System.out.println(empNames.get(4));
		
		//to know if the list contains a particular element
		System.out.println(empNames.contains("Geet2"));
		
		//finding index of n element
		System.out.println(empNames.indexOf("Geet2"));
		
		//iterate ArrayList
		Iterator<String> empIterator = empNames.iterator();
		
		while(empIterator.hasNext()){
			System.out.println(empIterator.next());
		}
	
 	}
}
