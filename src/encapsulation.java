
public class encapsulation {
	
	
		private int accountNo;
		private String AccountName;
		private float accBalance;
		private int phoneNumber;
		public String bankName; //you don't need setter & getter for public. When static public, you can access with the class name reference
		
//setter and getter for AccountNumber
		public int getAccountNumber() {
			return this.accountNo ;
		}
	
		//set method is not supposed to have a return type like int
		public void setAccountNumber(int accountNo) {
			this.accountNo = accountNo; 
		}

//setter and getter for AccountName
		public String getAccountName() {
			return this.AccountName ;
		}
		
		public void setAccountName(String AccountName) {
			this.AccountName = AccountName ;
		}
		
//setter and getter for accBalance
		public float getaccBalance() {
			return this.accBalance ;
		}
		
		public void setaccBalance(float accBalance) {
			this.accBalance = accBalance ;
		}
		
//setter and getter for phoneNumber
		public int getphoneNumber() {
			return this.phoneNumber ;
		}
		
		public void setphoneNumber(int phoneNumber) {
			this.phoneNumber = phoneNumber ;
		}
		
	
}
